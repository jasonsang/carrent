<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>会员中心</title>
    
	<link href="static/css/style_pc.css" rel="stylesheet">
	<link href="static/css/index.css" rel="stylesheet">
	
	<script src="static/js/jquery.min.js" type="text/javascript"></script>
	<script src="static/js/jquery.DB_gallery.js" type="text/javascript"></script>
	<script src="static/js/public_pc.js" type="text/javascript"></script>

  </head>
  
  <body>
  <jsp:include  page="head_nav_yes.jsp"/>
    <jsp:include  page="head.jsp"/>
    <jsp:include  page="left.jsp"/>
    	 <!-- 右侧内容开始 -->
	    <div class="user-bd-main_r">
	    	<!-- 单元内容开始 -->
                <div class="user-bd-main add-address">
                    <!-- 标题 -->
                    <div class="user-bd-main-title">基本资料</div>

                    <!--表单内容 -->
                    <div class="add-address-form">
                        <form name="userForm" id="userForm" action="javascript:void(0)" method="post">
                            <div class="formRow clearfix">
                                <span class="tag">姓名：</span>
                                <input class="sAddress" value="${user.custname}" name="name" type="text">
                            </div>
                            <div class="formRow clearfix">
                                <span class="tag">电话：</span>
                                <input class="dAddress" readOnly value="${user.phone}" name="tel" type="text">
                            </div>
                            <div class="formRow clearfix">
                                <span class="tag">地址：</span>
                                <input class="phoneNum"  value="${user.address}" name="address" type="text">
                            </div>
<%--                            <input class="btn-sub" type="submit" onclick="updateUserBase()" value="保存">--%>
                        </form>
                    </div>
                 </div>
             <!-- 单元内容结束-->
	    </div>
	    <div class="clear"></div>
	    <!-- 右侧内容结束 -->
   </div>
   <!-- 1200宽度结束-->
   <!-- 背景层结束-->
</div>
<%--		<script language="javascript">--%>
<%--			function updateUserBase(){--%>
<%--				var params= $('#userForm').serialize();--%>
<%--				$.ajax({--%>
<%--					url:"<%=basePath%>wx/wxUpdateUserInfo", //后台处理程序--%>
<%--					type:'post',         //数据发送方式--%>
<%--					dataType:'json',--%>
<%--					data:params,         //要传递的数据--%>
<%--					success:function(data){--%>
<%--						alert(data.tip);--%>
<%--						window.location.reload();--%>
<%--					}--%>
<%--				}); --%>
<%--			}--%>
<%--		</script>--%>

    <jsp:include  page="foot.jsp"/>
  </body>
</html>
