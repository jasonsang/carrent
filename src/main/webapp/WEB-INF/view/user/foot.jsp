<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<div class="clear footer">

		<div class="clear footer_copyright">
			<p class="left">${net.net.copyright}</p>
			<p class="right">如果您对${net.net.company}有任何意见，欢迎发送联系电话:${net.net.tel}</p>
		</div>
	</div>

<script>

$(document).ready(function(){
    $("#btn a").hover(function(){
            $(this).children("div.hides").show();
            $(this).children("img.shows").hide();
            $(this).animate({marginRight:'114px'},'0'); 
        
    },function(){ 
            	$(this).animate({marginRight:'0px'},'0',function(){
            		$(this).children("div.hides").hide();
					$(this).children("img.shows").show();
            	});
				    
    });
  
    $("#top_btn").click(function(){if(scroll=="off") return;$("html,body").animate({scrollTop: 0}, 600);});

	    //右侧导航 - 二维码
        $(".youhui").mouseover(function(){
            $(this).children(".2wm").show();
        })
        $(".youhui").mouseout(function(){
            $(this).children(".2wm").hide();
        });


});
</script>
<!-- 代码部分end -->