<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%--<jsp:useBean id="navigat" scope="page" class="com.weishang.bean.ReceptBean"/><!-- 主导航 -->
<jsp:useBean id="net" scope="page" class="com.weishang.bean.ReceptBean"/><!--网站基本信息-->
<jsp:useBean id="folder" scope="page" class="com.weishang.bean.ReceptBean"/><!-- 当前正在使用的模板 -->--%>
<div class="header">
	<div class="header_fun_bg"> 
		<div class="mainbox header_fun_info"> 
			尊敬的客户，欢迎您来到租车系统
			<div class="right">
				<c:if test="${ordinary_user==null}">
					<a href="index.action">首页</a>|
					<a href="<%=path%>/user/login.action">登陆</a>|
					<a href="<%=path%>/user/register.action">注册</a>
				</c:if>
				<c:if test="${ordinary_user!=null}">
					<a href="<%=path%>/toLogin.action">用户中心 </a>|
					<a href="<%=basePath%>user/index">退出</a>
				</c:if>
				|<a href="<%=path%>/login/toLogin.action">总后台 </a>

			</div>
		</div>
	</div>	
	<div class="header_menu_bg">
		<div class="mainbox header_menu_info">
			<%--<a href="<%=basePath%>" class="header_logo">
				<img src="<%=basePath%>${net.net.logo}" /></a>--%>
			<ul class="header_menu">
<%--				<jsp:setProperty property="addr" value="1" name="navigat"/>--%>
<%--				<jsp:setProperty property="host" value="<%=basePath%>" name="navigat"/>--%>
<%--				<c:forEach items="${navigat.receptList}" var="recept" varStatus="status">--%>
<%--					<li>--%>
<%--						<a href="${recept.modUrl}">${recept.name}</a>--%>
<%--					</li>--%>
<%--				</c:forEach>--%>
				<li class="app"><span class="header_menu_mobile"><a href="javascript:void(0)">手机版</a></span></li>
		<%--		<li class="phone"><img src="./static/default/images/header_logo_phone.jpg" /></li>--%>
				
			</ul>
		</div>
	</div>
