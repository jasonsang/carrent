<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
  <head>
    <base href="<%=basePath%>">
<%--    <jsp:useBean id="folder" scope="page" class="com.weishang.bean.ReceptBean"/><!-- 当前正在使用的模板 -->
 	<jsp:useBean id="slide" scope="page" class="com.weishang.bean.ReceptBean"/><!-- 幻灯片 -->
 	<jsp:useBean id="active" scope="page" class="com.weishang.my.bean.MyBean"/><!-- 精彩活动-->
 	<jsp:useBean id="news" scope="page" class="com.weishang.bean.ReceptBean"/><!-- 精彩活动-->
 	
 	<jsp:useBean id="bussinescar" scope="page" class="com.weishang.my.bean.MyBean"/><!--商务用车 -->
 	<jsp:useBean id="marrycar" scope="page" class="com.weishang.my.bean.MyBean"/><!--婚礼用车 -->
 	<jsp:useBean id="travelcar" scope="page" class="com.weishang.my.bean.MyBean"/><!--旅游用车 -->
 	<jsp:useBean id="planecar" scope="page" class="com.weishang.my.bean.MyBean"/><!--接送机用车 -->
 	
 	<jsp:useBean id="auntList" scope="page" class="com.weishang.my.bean.MyBean"/><!--师机 -->--%>
 	
    <title>${recept.title}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="">
	<meta http-equiv="description" content="">

	<link rel="stylesheet" type="text/css" href="<%=path%>/static/css/user.css">
  </head>
  
  <body>
  	<jsp:include  page="head.jsp"/>
  	<div class="header_banner">
			<div class="header_banner_box">
			  <div class="bd">
				  <div class="tempWrap" style="overflow:hidden; position:relative; width:1886px"><ul class="header_banner_list" style="width: 7544px; position: relative; overflow: hidden; padding: 0px; margin: 0px; left: -3772px;"><li class="clone" style="float: left; width: 1886px;">
					  <a href="" style="background: url(<%=path%>/static/images/bg2.jpg) no-repeat center top;"></a>
				  </li>

					  <li style="float: left; width: 1886px;">
						  <a href="" style="background: url(<%=path%>/static/images/bg1.jpg) no-repeat center top;"></a>
					  </li>

					  <li style="float: left; width: 1886px;">
						  <a href="" style="background: url(<%=path%>/static/images/bg2.jpg) no-repeat center top;"></a>
					  </li>

					  <li class="clone" style="float: left; width: 1886px;">
						  <a href="" style="background: url(<%=path%>/static/images/bg1.jpg) no-repeat center top;"></a>
					  </li></ul></div>
				</div>
			  <div class="hd"><ul></ul></div>
			</div>
			<%--<form action="<%=basePath%>front?tag=goFastOrder" method="post" name="fast_form" id="fast_form">
				<div class="header_search text-center">

					<input type="hidden" name="store_id" id="store_id" value=""/>
					<input type="hidden" name="type_id" id="type_id" value=""/>
					<ul>
						<li class="left search_info">
							<div class="left down_list1">选择门店<!--<span></span>--></div>
							<div class="left down_list2" onClick="showList()">选择服务类型</div>
							<input type="text" name="time" id="time" class="left down_list3" onClick="WdatePicker()" placeholder="选择时间">
							<ul class="list_store">
								<jsp:useBean id="storeList" scope="page" class="com.weishang.my.bean.MyBean"/><!--商务用车 -->
								<c:forEach items="${storeList.storeList}" var="store" varStatus="status">
									<li var="${store.id}">${store.name}</li>
								</c:forEach>
							</ul>
							<ul class="list_type">
								<jsp:useBean id="catList" scope="page" class="com.weishang.my.bean.MyBean"/><!--商务用车 -->
								<c:forEach items="${catList.catList}" var="cat" varStatus="status">
									<li var="${cat.id}">${cat.name}</li>
								</c:forEach>
							</ul>
						</li>

						<buttun class="left btn_search text-center" id="search_submit" onclick="form_smt()">快速订车</buttun>
					</ul>
				</div>
			</form>--%>
		</div>
		<ul class="header_discribe">
			<li>
				<img class="left" src="<%=path%>/static/images/discribe1.jpg" />
				<a href="#" class="left header_discribe_info">
					<h3>接送机场</h3>
					<p>无需任何手续，方便快捷提前到达指定地点等候</p>
				</a>
			</li>
			<li>
				<img class="left" src="<%=path%>/static/images/discribe2.jpg" />
				<a href="#" class="left header_discribe_info">
					<h3>企业服务</h3>
					<p>承接各种大型会议用车专业商务接待团队</p>
				</a>
			</li>
			<li>
				<img class="left" src="<%=path%>/static/images/discribe3.jpg" />
				<a href="#" class="left header_discribe_info">
					<h3>婚期豪车</h3>
					<p>多款豪华跑车供您选择高中低档搭配套餐</p>
				</a>
			</li>
			<li>
				<img class="left" src="<%=path%>/static/images/discribe4.jpg" />
				<a href="#" class="left header_discribe_info">
					<h3>旅游越野</h3>
					<p>为您量身打造私人线路领略不同旅途风景</p>
				</a>
			</li>
		</ul>
	</div>
	<div class="clear contain">
		<div class="contain_models" style="height:700px;">
			<div class="contain_models_bg">
				<label class="left">热租车型</br><em>HOT RENT CAR</em></label>
				<ul class="contain_models_nav right">
                    <li><a href="javascript:void(0)" class="contain_models_action">轿车</a></li>
					<li><a href="javascript:void(0)">SUV</a></li>
					<li><a href="javascript:void(0)">跑车</a></li>
					<li><a href="javascript:void(0)">新能源</a></li>
				</ul>
				<div class="contain_models_leftbg"></div>
				<div class="contain_models_rightbg"></div>
			</div>
			<%--轿车--%>
			<div class="parBd">
				<div class="models_slidBox">
					<a href="javascript:void(0)" class="prev"></a>
					<div class="slide">
						<ul class="contain_models_list">
<%--							<jsp:setProperty property="pageNo" value="1" name="bussinescar"/>--%>
<%--							<jsp:setProperty property="pageSize" value="6" name="bussinescar"/>--%>
<%--							<jsp:setProperty property="cat_id" value="1" name="bussinescar"/>--%>
							<c:forEach items="${car}" var="car" varStatus="status">
								<li>
									<a href="<%=basePath%>front/carDetail/${car.carnumber}.html">
<%--				                        <div class="p1z">${bus_car.price}${bus_car.rentprice}</div>--%>
				                        <div class="img_zs"><img src="<%=basePath%>${car.carimg}" /></div>
				                        <div class="em_z">${car.description}</div>
			                        </a>
		                        </li>
	                        </c:forEach>
						</ul>
					</div>
					<a href="javascript:void(0)" class="next"></a>
				</div>

				<%--SUV--%>
<%--				<div class="models_slidBox">--%>
<%--					<a href="javascript:void(0)" class="prev"></a>--%>
<%--					<div class="slide">--%>
<%--						&lt;%&ndash;<ul class="contain_models_list">--%>
<%--	                        <jsp:setProperty property="pageNo" value="1" name="marrycar"/>--%>
<%--							<jsp:setProperty property="pageSize" value="6" name="marrycar"/>--%>
<%--							<jsp:setProperty property="cat_id" value="15" name="marrycar"/>--%>
<%--							<c:forEach items="${marrycar.goodsListByCat}" var="goods" varStatus="status">--%>
<%--								<li>--%>
<%--									<a href="<%=basePath%>front/goodsDetail/${goods.goodsId}.html">--%>
<%--				                        <div class="p1z">${goods.shopPrice}${goods.category.measure_unit}</div>--%>
<%--				                        <div class="img_zs"><img src="<%=basePath%>${goods.originalImg}" /></div>--%>
<%--				                        <div class="em_z">${goods.goodsName}</div>--%>
<%--			                        </a>--%>
<%--		                        </li>--%>
<%--	                        </c:forEach>--%>
<%--						</ul>&ndash;%&gt;--%>
<%--					</div>--%>
<%--					<a href="javascript:void(0)" class="next"></a>--%>
<%--				</div>--%>

<%--				<div class="models_slidBox">--%>
<%--					<a href="javascript:void(0)" class="prev"></a>--%>
<%--					<div class="slide">--%>
<%--					&lt;%&ndash;	<ul class="contain_models_list">--%>
<%--							<jsp:setProperty property="pageNo" value="1" name="travelcar"/>--%>
<%--							<jsp:setProperty property="pageSize" value="6" name="travelcar"/>--%>
<%--							<jsp:setProperty property="cat_id" value="16" name="travelcar"/>--%>
<%--							<c:forEach items="${travelcar.goodsListByCat}" var="goods" varStatus="status">--%>
<%--								<li>--%>
<%--									<a href="<%=basePath%>front/goodsDetail/${goods.goodsId}.html">--%>
<%--				                        <div class="p1z">${goods.shopPrice}${goods.category.measure_unit}</div>--%>
<%--				                        <div class="img_zs"><img src="<%=basePath%>${goods.originalImg}" /></div>--%>
<%--				                        <div class="em_z">${goods.goodsName}</div>--%>
<%--			                        </a>--%>
<%--		                        </li>--%>
<%--	                        </c:forEach>--%>
<%--						</ul>&ndash;%&gt;--%>
<%--					</div>--%>
<%--					<a href="javascript:void(0)" class="next"></a>--%>
<%--				</div>--%>
<%--				--%>
<%--				<div class="models_slidBox">--%>
<%--					<a href="javascript:void(0)" class="prev"></a>--%>
<%--					<div class="slide">--%>
<%--					&lt;%&ndash;	<ul class="contain_models_list">--%>
<%--							<jsp:setProperty property="pageNo" value="1" name="planecar"/>--%>
<%--							<jsp:setProperty property="pageSize" value="6" name="planecar"/>--%>
<%--							<jsp:setProperty property="cat_id" value="17" name="planecar"/>--%>
<%--							<c:forEach items="${planecar.goodsListByCat}" var="goods" varStatus="status">--%>
<%--								<li>--%>
<%--									<a href="<%=basePath%>front/goodsDetail/${goods.goodsId}.html">--%>
<%--				                        <div class="p1z">${goods.shopPrice}${goods.category.measure_unit}</div>--%>
<%--				                        <div class="img_zs"><img src="<%=basePath%>${goods.originalImg}" /></div>--%>
<%--				                        <div class="em_z">${goods.goodsName}</div>--%>
<%--			                        </a>--%>
<%--		                        </li>--%>
<%--	                        </c:forEach>--%>
<%--						</ul>&ndash;%&gt;--%>
<%--					</div>--%>
<%--					<a href="javascript:void(0)" class="next"></a>--%>
<%--				</div>--%>
				
			</div>
		</div>



			<ul class="clear rent_flow">
				<li>
					<span class="left num_icon">1</span>
					<p class="left mien_tit">预订车辆</br><font>提前为您预留</font></p>					
				</li>
				<li style="border-left:0px solid red">
					<span class="left num_icon">2</span>
					<p class="left mien_tit">签订合同</br><font>双方共同验车</font></p>
				</li>
				<li style="border-left:0px solid red">
					<span class="left num_icon">3</span>
					<p class="left mien_tit">开心旅途</br><font>一路为您保驾护航</font></p>
				</li>
				<li style="border-left:0px solid red">
					<span class="left num_icon">4</span>
					<p class="left mien_tit">退还车辆</br><font>完成租车使用</font></p>
				</li>
			</ul>
			
			<div class="clear"></div>
		</div>
	</div>
	<script type="text/javascript">


		var tableIns;
		layui.use(['jquery', 'layer', 'form', 'table', 'upload'], function () {
			var $ = layui.jquery;
			var layer = layui.layer;
			var form = layui.form;
			var table = layui.table;
			var dtree = layui.dtree;
			var upload = layui.upload;
			//渲染数据表格
			tableIns = table.render({
				elem: '#carTable'   //渲染的目标对象
				, url: '<%=path%>/car/loadAllCar.action' //数据接口
				, title: '车辆数据表'//数据导出来的标题
				, toolbar: "#carToolBar"   //表格的工具条
				, height: 'full-205'
				, cellMinWidth: 100 //设置列的最小默认宽度
				, page: true  //是否启用分页
				, cols: [[   //列表数据
					{type: 'checkbox', fixed: 'left'}
					, {field: 'carnumber', title: '车牌号', align: 'center', width: '110'}
					, {field: 'cartype', title: '车辆类型', align: 'center', width: '90'}
					, {field: 'color', title: '车辆颜色', align: 'center', width: '90'}
					, {field: 'price', title: '车辆价格', align: 'center', width: '90'}
					, {field: 'rentprice', title: '出租价格', align: 'center', width: '90'}
					, {field: 'deposit', title: '出租押金', align: 'center', width: '90'}
					, {
						field: 'isrenting', title: '出租状态', align: 'center', width: '90', templet: function (d) {
							return d.isrenting == '1' ? '<font color=blue>已出租</font>' : '<font color=red>未出租</font>';
						}
					}
					, {field: 'description', title: '车辆描述', align: 'center', width: '150'}
					, {
						field: 'carimg', title: '缩略图', align: 'center', width: '80', templet: function (d) {
							return "<img width=40 height=40 src=<%=path%>/file/downloadShowFile.action?path="+ d.carimg + "/>";
						}
					}
					, {field: 'createtime', title: '录入时间', align: 'center', width: '165'}
					, {fixed: 'right', title: '操作', toolbar: '#carBar', align: 'center', width: '190'}
				]],
				done: function (data, curr, count) {
					//不是第一页时，如果当前返回的数据为0那么就返回上一页
					if (data.data.length == 0 && curr != 1) {
						tableIns.reload({
							page: {
								curr: curr - 1
							}
						})
					}
				}
			});


	</script>
  	<jsp:include  page="foot.jsp"/>
    <script type="text/javascript" src="<%=path%>/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=path%>/static/js//My97DatePicker/calendar.js"></script>
	<script type="text/javascript" src="<%=path%>/static/js//My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/static/js//jquery.SuperSlide.2.1.1.js"></script>
	<script type="text/javascript" src="<%=path%>/static/js//main_js.js"></script>
	<script type="text/javascript">
		function form_smt(){
			var store_id=document.getElementById("store_id").value;
			var type_id=document.getElementById("type_id").value;
			var time=document.getElementById("time").value;
			if(store_id==""||store_id==null){
	         	alert("请选择门店！");
	        }else if(type_id==""||type_id==null){
	         	alert("请选择服务类型！");
	        }else if(time==""||time==null){
	         	alert("请选择时间！");
	        }else{
	        	document.forms["fast_form"].submit();
	        }
		}
	</script>
  </body>
</html>
