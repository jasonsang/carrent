package com.carrent.user.mapper;

import com.carrent.bus.domain.Car;
import com.carrent.user.domain.BaseUserBean;

import javax.annotation.Resource;

/**
 * (BusCustomer)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-17 00:44:40
 */
@Resource
public interface BaseDao {

    /**
     * 查找用户名和密码
     * @param username 登录用户名
//     * @param password 密码
     * @return
     */
    BaseUserBean findByUsername(String username, String password);

    Car selectByPrimaryKey(String opername);
    //List<Car> queryAllCar(CarVo carVo);
}