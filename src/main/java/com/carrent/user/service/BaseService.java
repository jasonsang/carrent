package com.carrent.user.service;

import com.carrent.bus.domain.Car;
import com.carrent.user.domain.BaseUserBean;

/**
 * (BusCustomer)表服务接口
 *
 * @author makejava
 * @since 2020-05-17 00:44:40
 */
public interface BaseService {

    //检验用户登录
    BaseUserBean checkLogin(String username, String password);
    Car selectByPrimaryKey(String opername);

    /**
     * 查询所有车辆
     * @param carVo
     * @return
     */
//    DataGridView queryAllCar(CarVo carVo);

//    List showCar();
}