package com.carrent.user.service.impl;

import com.carrent.bus.domain.Car;
import com.carrent.user.domain.BaseUserBean;
import com.carrent.user.mapper.BaseDao;
import com.carrent.user.service.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BaseServiceImpl implements BaseService {

	@Autowired
	private BaseDao baseDao;

	/* 
	 * 检验用户登录业务
	 * 
	 */
	@Override
	public BaseUserBean checkLogin(String username, String password) {
		
		BaseUserBean user = baseDao.findByUsername(username,password);
		if(user != null && user.getPassword().equals(password)){
		
			return user;
		}
		return null;
	}

	@Override
	public Car selectByPrimaryKey(String opername) {
//		BaseUserBean user = baseDao.selectByPrimaryKey("");


		return null;
	}


//	@Override
//	public DataGridView queryAllCar(CarVo carVo) {
//		Page<Object> page = PageHelper.startPage(carVo.getPage(),carVo.getLimit());
//		List<Car> data = baseDao.queryAllCar(carVo);
//
//		return new DataGridView(page.getTotal(),data);
//	}

//	@Override
//	public BaseCarBean showCar() {
//		List carlist = new ArrayList();
//		BaseCarBean baseCarBean = new BaseCarBean();
//		baseCarBean.getDescription();
//		carlist.add();
//
//		return carlist;
//	}

}
