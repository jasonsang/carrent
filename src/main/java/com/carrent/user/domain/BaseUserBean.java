package com.carrent.user.domain;

import java.util.Date;
import java.io.Serializable;
import java.util.Objects;

/**
 * (BusCustomer)实体类
 *
 * @author makejava
 * @since 2020-05-17 11:52:24
 */
public class BaseUserBean implements Serializable {
    private static final long serialVersionUID = -96865053189200101L;
    /**
    * 身份证
    */
    private String identity;
    /**
    * 用户名
    */
    private String username;
    /**
    * 密码
    */
    private String password;
    /**
    * 姓名
    */
    private String custname;
    /**
    * 性别
    */
    private Integer sex;
    /**
    * 地址
    */
    private String address;
    /**
    * 电话
    */
    private String phone;
    /**
    * 职位
    */
    private String career;
    
    private Date createtime;
    /**
    * 余额
    */
    private Integer money;


    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }


    public BaseUserBean(String identity, String username, String password, String custname, Integer sex, String address, String phone, String career, Date createtime, Integer money) {
        this.identity = identity;
        this.username = username;
        this.password = password;
        this.custname = custname;
        this.sex = sex;
        this.address = address;
        this.phone = phone;
        this.career = career;
        this.createtime = createtime;
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseUserBean baseUserBean = (BaseUserBean) o;
        return Objects.equals(identity, baseUserBean.identity) &&
                Objects.equals(username, baseUserBean.username) &&
                Objects.equals(password, baseUserBean.password) &&
                Objects.equals(custname, baseUserBean.custname) &&
                Objects.equals(sex, baseUserBean.sex) &&
                Objects.equals(address, baseUserBean.address) &&
                Objects.equals(phone, baseUserBean.phone) &&
                Objects.equals(career, baseUserBean.career) &&
                Objects.equals(createtime, baseUserBean.createtime) &&
                Objects.equals(money, baseUserBean.money);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identity, username, password, custname, sex, address, phone, career, createtime, money);
    }
}
