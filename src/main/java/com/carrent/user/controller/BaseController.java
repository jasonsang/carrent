package com.carrent.user.controller;

import com.carrent.user.domain.BaseCarBean;
import com.carrent.user.domain.BaseUserBean;
import com.carrent.user.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller

//这里用了@SessionAttributes，可以直接把model中的user(也就key)放入其中
//这样保证了session中存在user这个对象
//@SessionAttributes("user")
public class BaseController {
    @Autowired
    private BaseService baseService;

    //@Resource
   // private IUserInfoService userInfoService;
//    private UserLoginImpl userLogin;
//    @Autowired
//    public BaseController(UserLoginImpl userLogin){
//        this.userLogin = userLogin;
//    }
//

    @RequestMapping(value = "index.action")
    public String showCarIndex(BaseCarBean baseCarBean, Model model){
        //baseCarBean;
//        model = (Model) baseService.queryAllCar(carVo);
//        baseCarBean = baseService.selectByPrimaryKey();

        System.out.println("打开首页成功"+model);
        model.addAttribute("car",model);
        return "user/index";
    }

//    @RequestMapping("/{page}")
//    public String degitalPage(String page){
//        page = "user/success";
//        return page;
//    }


    //请求映射
    @RequestMapping(value="user/login.action",method= RequestMethod.GET)
    public String login(Model model) {
        String name = "sang";
        model.addAttribute("msg",name);
        return "user/user_login";
    }


    /*登录*/
    //表单提交过来的路径
    @RequestMapping("user/checkLogin.action")
    public String checkLogin(BaseUserBean user, Model model){
//         //   UserBean user, Model model
////        //调用service方法
////        user = baseService.checkLogin(user.getUsername(), user.getPassword());
////        //若有user则添加到model里并且跳转到成功页面
////        if(user != null){
////            model.addAttribute("user",user);
////            return "user/index";
////        }
        return "user/user_login";
    }






    @RequestMapping(value="/register.action",method= RequestMethod.GET)
    public String register(Model model) {
        String name = "sang";
        model.addAttribute("msg",name);
        return "user/user_register";
    }



}
